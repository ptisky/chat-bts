package bts;

import java.util.Date;

public class Manutentionnaire extends Employe{

	final double SALAIRE_HORAIRE = 10.9;
	private int nbHeure;
	
	public Manutentionnaire(String mNom, String mPrenom, int mAge, Date mdate, int pNbHeure)
	{
		super(mNom, mPrenom, mAge, mdate);
		this.nbHeure=pNbHeure;
	}
	double calculerSalaire()
	{
		return SALAIRE_HORAIRE*nbHeure;
	}
	
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	
	
}
