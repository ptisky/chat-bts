<html><head><title>CRUD Tutorial - Customer's list</title></head><body>
<?php

define('DEBUG', true);											// Debug mode
define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/');		// Root path of your PrestaShop store
define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	// Auth key (Get it in your Back Office)
require_once('./PSWebServiceLibrary.php');
// On appel le webservice
try
{
	$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
	
	// La ressource que l'on veux
	$opt['resource'] = 'customers';
	$opt2['resource'] = 'products';
	
	// Call
	$xml = $webService->get($opt);
	$xml2 = $webService->get($opt2);
	
	$resources = $xml->customers->children();
	$resources2 = $xml2->products->children();
}
catch (PrestaShopWebserviceException $e)
{
	// On affiche les erreurs
	$trace = $e->getTrace();
	if ($trace[0]['args'][0] == 404) echo 'Bad ID';
	else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
	else echo 'Other error';
}


// Le titre
echo "<h1>List Client</h1>";
echo '<table border="5">';
if (isset($resources))
{
		echo '<tr><th>Id client</th></tr>';
		
		foreach ($resources as $resource)
		{
			// Iterates on the found IDs
			echo '<tr><td>'.$resource->attributes().'</td></tr>';
		}
}
echo '</table>';


echo "<h1>List produit</h1>";
echo '<table border="5">';
if (isset($resources2))
{
		echo '<tr><th>Id produit</th></tr>';
		
		foreach ($resources2 as $resource2)
		{
			// Iterates on the found IDs
			echo '<tr><td>'.$resource2->attributes().'</td></td>';
		}
}
echo '</table>';




?>
</body></html>