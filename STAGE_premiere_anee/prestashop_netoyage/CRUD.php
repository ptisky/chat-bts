<html><head><title>CRUD Tutorial - Customer's list</title></head><body>
<?php

define('DEBUG', true);	
define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/');
define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	
require_once('./PSWebServiceLibrary.php');
// On appel le webservice
try
{
	$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
	
	// La ressource que l'on veux
	$opt1['resource'] = 'customers';
	$opt1['display'] = 'full';
	$opt2['resource'] = 'products';
	$opt2['display'] = 'full';
	$opt3['resource'] = 'categories';
	$opt3['display'] = 'full';
	$opt4['resource'] = 'countries';
	$opt4['display'] = 'full';
	$opt5['resource'] = 'currencies';
	$opt5['display'] = 'full';		
	$opt6['resource'] = 'addresses';
	$opt6['display'] = 'full';	
	$opt7['resource'] = 'carriers';
	$opt7['display'] = 'full';
	$opt8['resource'] = 'cart_rules';
	$opt8['display'] = 'full';
	$opt9['resource'] = 'carts';
	$opt9['display'] = 'full';
	$opt10['resource'] = 'combinations';
	$opt10['display'] = 'full';
	$opt11['resource'] = 'configurations';
	$opt11['display'] = 'full';	
	$opt12['resource'] = 'contacts';
	$opt12['display'] = 'full';
	$opt13['resource'] = 'content_management_system';
	$opt13['display'] = 'full';
	$opt14['resource'] = 'customer_messages';
	$opt14['display'] = 'full';
	$opt15['resource'] = 'customer_threads';
	$opt15['display'] = 'full';
	$opt16['resource'] = 'customizations';
	$opt16['display'] = 'full';	
	$opt17['resource'] = 'deliveries';
	$opt17['display'] = 'full';
	$opt18['resource'] = 'employees';
	$opt18['display'] = 'full';
	$opt19['resource'] = 'groups';
	$opt19['display'] = 'full';
	$opt20['resource'] = 'guests';
	$opt20['display'] = 'full';
	$opt21['resource'] = 'image_types';
	$opt21['display'] = 'full';	
	$opt22['resource'] = 'images';
	$opt22['display'] = 'full';
	$opt23['resource'] = 'manufacturers';
	$opt23['display'] = 'full';
	$opt24['resource'] = 'order_carriers';
	$opt24['display'] = 'full';
	$opt25['resource'] = 'order_details';
	$opt25['display'] = 'full';
	$opt26['resource'] = 'order_discounts';
	$opt26['display'] = 'full';	
	$opt27['resource'] = 'order_histories';
	$opt27['display'] = 'full';
	$opt28['resource'] = 'order_invoices';
	$opt28['display'] = 'full';
	$opt29['resource'] = 'order_payments';
	$opt29['display'] = 'full';
	$opt30['resource'] = 'order_slip';
	$opt30['display'] = 'full';
	$opt31['resource'] = 'order_states';
	$opt31['display'] = 'full';	
	$opt32['resource'] = 'orders';
	$opt32['display'] = 'full';
	$opt33['resource'] = 'price_ranges';
	$opt33['display'] = 'full';
	$opt34['resource'] = 'product_customization_fields';
	$opt34['display'] = 'full';
	$opt35['resource'] = 'product_feature_values';
	$opt35['display'] = 'full';
	$opt36['resource'] = 'product_features';
	$opt36['display'] = 'full';	
	$opt37['resource'] = 'product_option_values';
	$opt37['display'] = 'full';
	$opt38['resource'] = 'product_options';
	$opt38['display'] = 'full';
	$opt39['resource'] = 'product_suppliers';
	$opt39['display'] = 'full';
	$opt40['resource'] = 'shop_groups';
	$opt40['display'] = 'full';
	$opt41['resource'] = 'shop_urls';
	$opt41['display'] = 'full';
	$opt42['resource'] = 'shops';
	$opt42['display'] = 'full';
	$opt43['resource'] = 'specific_price_rules';
	$opt43['display'] = 'full';
	$opt44['resource'] = 'specific_prices';
	$opt44['display'] = 'full';	
	$opt45['resource'] = 'states';
	$opt45['display'] = 'full';
	$opt46['resource'] = 'stock_availables';
	$opt46['display'] = 'full';
	$opt47['resource'] = 'stock_movement_reasons';
	$opt47['display'] = 'full';
	$opt48['resource'] = 'stock_movements';
	$opt48['display'] = 'full';
	$opt49['resource'] = 'stocks';
	$opt49['display'] = 'full';	
	$opt50['resource'] = 'stores';
	$opt50['display'] = 'full';
	$opt51['resource'] = 'suppliers';
	$opt51['display'] = 'full';
	$opt52['resource'] = 'supply_order_details';
	$opt52['display'] = 'full';
	$opt53['resource'] = 'supply_order_histories';
	$opt53['display'] = 'full';
	$opt54['resource'] = 'supply_order_receipt_histories';
	$opt54['display'] = 'full';	
	$opt55['resource'] = 'supply_orders';
	$opt55['display'] = 'full';
	$opt56['resource'] = 'tags';
	$opt56['display'] = 'full';
	$opt57['resource'] = 'tax_rule_groups';
	$opt57['display'] = 'full';
	$opt58['resource'] = 'tax_rules';
	$opt58['display'] = 'full';
	$opt59['resource'] = 'taxes';
	$opt59['display'] = 'full';	
	$opt60['resource'] = 'warehouse_product_locations';
	$opt60['display'] = 'full';	
	$opt61['resource'] = 'warehouses';
	$opt61['display'] = 'full';
	$opt62['resource'] = 'weight_ranges';
	$opt62['display'] = 'full';
	$opt63['resource'] = 'zones';
	$opt63['display'] = 'full';
	
	// Call
	$xml1 = $webService->get($opt1);
	$xml2 = $webService->get($opt2);
	$xml3 = $webService->get($opt3);
	$xml4 = $webService->get($opt4);
	$xml5 = $webService->get($opt5);
	$xml6 = $webService->get($opt6);
	$xml7 = $webService->get($opt7);
	$xml8 = $webService->get($opt8);
	$xml9 = $webService->get($opt9);
	$xml10 = $webService->get($opt10);
	$xml11 = $webService->get($opt11);
	$xml12 = $webService->get($opt12);
	$xml13 = $webService->get($opt13);
	$xml14 = $webService->get($opt14);
	$xml15 = $webService->get($opt15);
	$xml16 = $webService->get($opt16);
	$xml17 = $webService->get($opt17);
	$xml18 = $webService->get($opt18);
	$xml19 = $webService->get($opt19);
	$xml20 = $webService->get($opt20);
	$xml21 = $webService->get($opt21);
	$xml22 = $webService->get($opt22);
	$xml23 = $webService->get($opt23);
	$xml24 = $webService->get($opt24);
	$xml25 = $webService->get($opt25);
	$xml26 = $webService->get($opt26);
	$xml27 = $webService->get($opt27);
	$xml28 = $webService->get($opt28);
	$xml29 = $webService->get($opt29);
	$xml30 = $webService->get($opt30);
	$xml31 = $webService->get($opt31);
	$xml32 = $webService->get($opt32);
	$xml33 = $webService->get($opt33);
	$xml34 = $webService->get($opt34);
	$xml35 = $webService->get($opt35);
	$xml36 = $webService->get($opt36);
	$xml37 = $webService->get($opt37);
	$xml38 = $webService->get($opt38);
	$xml39 = $webService->get($opt39);
	$xml40 = $webService->get($opt40);
	$xml41 = $webService->get($opt41);
	$xml42 = $webService->get($opt42);
	$xml43 = $webService->get($opt43);
	$xml44 = $webService->get($opt44);
	$xml45 = $webService->get($opt45);
	$xml46 = $webService->get($opt46);
	$xml47 = $webService->get($opt47);
	$xml48 = $webService->get($opt48);
	$xml49 = $webService->get($opt49);
	$xml50 = $webService->get($opt50);
	$xml51 = $webService->get($opt51);
	$xml52 = $webService->get($opt52);
	$xml53 = $webService->get($opt53);
	$xml54 = $webService->get($opt54);
	$xml55 = $webService->get($opt55);
	$xml56 = $webService->get($opt56);
	$xml57 = $webService->get($opt57);
	$xml58 = $webService->get($opt58);
	$xml59 = $webService->get($opt59);
	$xml60 = $webService->get($opt60);
	$xml61 = $webService->get($opt61);
	$xml62 = $webService->get($opt62);
	$xml63 = $webService->get($opt63);

	
	$resources = $xml1->customers->children();
	$resources2 = $xml2->products->children();
	$resources3 = $xml3->categories->children();
	$resources4 = $xml4->countries->children();
	$resources5 = $xml5->currencies->children();
	$resources6 = $xml6->addresses->children();
	$resources7 = $xml7->carriers->children();
	$resources8 = $xml8->cart_rules->children();
	$resources9 = $xml9->carts->children();
	$resources10 = $xml10->combinations->children();
	$resources11 = $xml11->configurations->children();
	$resources12 = $xml12->contacts->children();
	$resources13 = $xml13->content_management_system->children();
	$resources14 = $xml14->customer_messages->children();
	$resources15 = $xml15->customer_threads->children();
	$resources16 = $xml16->customizations->children();
	$resources17 = $xml17->deliveries->children();
	$resources18 = $xml18->employees->children();
	$resources19 = $xml19->groups->children();
	$resources20 = $xml20->guests->children();
	$resources21 = $xml21->image_types->children();
	$resources22 = $xml22->images->children();
	$resources23 = $xml23->manufacturers->children();
	$resources24 = $xml24->order_carriers->children();
	$resources25 = $xml25->order_details->children();
	$resources26 = $xml26->order_discounts->children();
	$resources27 = $xml27->order_histories->children();
	$resources28 = $xml28->order_invoices->children();
	$resources29 = $xml29->order_payments->children();
	$resources30 = $xml30->order_slip->children();
	$resources31 = $xml31->order_states->children();
	$resources32 = $xml32->orders->children();
	$resources33 = $xml33->price_ranges->children();
	$resources34 = $xml34->product_customization_fields->children();
	$resources35 = $xml35->product_feature_values->children();
	$resources36 = $xml36->product_features->children();
	$resources37 = $xml37->product_option_values->children();
	$resources38 = $xml38->product_options->children();
	$resources39 = $xml39->product_suppliers->children();
	$resources40 = $xml40->shop_groups->children();
	$resources41 = $xml41->shop_urls->children();
	$resources42 = $xml42->shops->children();
	$resources43 = $xml43->specific_price_rules->children();
	$resources44 = $xml44->categories->children();
	$resources45 = $xml45->specific_prices->children();
	$resources46 = $xml46->states->children();
	$resources47 = $xml47->stock_availables->children();
	$resources48 = $xml48->stock_movement_reasons->children();
	$resources49 = $xml49->stock_movements->children();
	$resources50 = $xml50->stocks->children();
	$resources51 = $xml51->stores->children();
	$resources52 = $xml52->suppliers->children();
	$resources53 = $xml53->supply_order_details->children();
	$resources54 = $xml54->supply_order_histories->children();
	$resources55 = $xml55->supply_order_receipt_histories->children();
	$resources56 = $xml56->supply_orders->children();
	$resources57 = $xml57->tags->children();
	$resources58 = $xml58->tax_rule_groups->children();
	$resources59 = $xml59->tax_rules->children();
	$resources60 = $xml60->taxes->children();
	$resources61 = $xml61->warehouse_product_locations->children();
	$resources62 = $xml62->warehouses->children();
	$resources63 = $xml63->weight_ranges->children();

}
catch (PrestaShopWebserviceException $e)
{
	// On affiche les erreurs
	$trace = $e->getTrace();
	if ($trace[0]['args'][0] == 404) echo 'Bad ID';
	else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
	else echo 'Other error';
}


?>