<html><head>
		<title>CRUD modifer un item</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    </head><body>
	     <div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="R-CRUD_index.php">
								<i class="fa fa-star fa-fw"></i>index
							</a>
						</li>
					</ul> 
				</div>
			</div>
		</div>
        <div class="cover">
            <div class="cover-image" style="background-image : url('bg.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>web service</h1>
                        <p class="text-danger">prestashop web service J4GUAR</p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
<?php
				//connection de l'api au webservice
				define('DEBUG', true);	// debeuguage 
				define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/');//lien de la boutique
				define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	//clé d'authentification
				require_once('./PSWebServiceLibrary.php'); //lien de la librairy du web service
				// On appel le webservice
				try
				{
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
					$opt = array('resource' => 'customers');//met les customers dans un tableau
					if (isset($_GET['id']))//si $_GET['id'] existe
						$opt['id'] = $_GET['id']; 
					$xml = $webService->get($opt);
					$resources = $xml->children()->children();//récupere le xml dans $resources
				}
				
				catch (PrestaShopWebserviceException $e)
				{
					$trace = $e->getTrace();//on affiche les erreurs
					if ($trace[0]['args'][0] == 404) echo 'Bad ID';
					else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
					else echo 'Other error<br />'.$e->getMessage();
				}
				
				if (isset($_GET['id']) && isset($_POST['id'])) //si $_GET['id'] et $_POST['id'] existe :
				{
					foreach ($resources as $nodeKey => $node)//créer une boucle qui met $ressource dans un tableau (avant)
					{
						$resources->$nodeKey = $_POST[$nodeKey];//permet la mise à jour XML avec de nouvelles valeurs
					}
					
					try
					{
						$opt = array('resource' => 'customers');//recupere les ressources de customers dans un tableau
						$opt['putXml'] = $xml->asXML();//création d'un xml
						$opt['id'] = $_GET['id'];
						$xml = $webService->edit($opt);//envoie le xml
						echo "Successfully updated.";
					}
					
					catch (PrestaShopWebserviceException $ex)
					{
						$trace = $ex->getTrace();//affiche les erreurs
						if ($trace[0]['args'][0] == 404) echo 'Bad ID';
						else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
						else echo 'Other error<br />'.$ex->getMessage();
					}
				}
				
				echo '<h1>Customer\'s ';//titre
				if (isset($_GET['id'])) echo 'Update';//si $_GET['id']
				
				else echo 'List';
				
				echo '</h1>';//fin titre

				if (isset($_GET['id']))//si $_GET['id'] existe
					echo '<a href="?">Returner a la liste</a>';//on créer un bouton de retour
					
				if (isset($_GET['id'])) //Si $_GET['id'] existe
					echo '<form method="POST" action="?id='.$_GET['id'].'">';//on créer un formulaire html
					
				echo '<table border="5">'; //creation tableau html
				if (isset($resources))//si $ressource existe
				{
					echo '<tr>';
					
					if (!isset($_GET['id']))//si $_GET['id'] existe 
					{
						echo '<th>Id</th><th>Plus</th></tr>';
						foreach ($resources as $resource)//créer une boucle 
						{
							echo '<td>'.$resource->attributes().'</td><td>'.//Affiche l'id des customers
							'<a href="?id='.$resource->attributes().'">Modifier</a>&nbsp;'.//renvoie a la page du customers
							'</td></tr>';
						}
					}
					else
					{
						echo '</tr>';
						foreach ($resources as $key => $resource)//créer une boucle
						{
							echo '<tr><th>'.$key.'</th><td>';//affiche les elements de customers
							echo '<input type="text" name="'.$key.'" value="'.$resource.'"/>';
							echo '</td></tr>';
						}
					}
				}
				echo '</table><br/>';//fin du tableau
				if (isset($_GET['id']))//si $_GET['id'] existe
					echo '<input type="submit" value="Modifier"></form>';//envoie le formulaire
?>
</body></html>