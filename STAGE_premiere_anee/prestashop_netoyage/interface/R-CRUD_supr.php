<html><head>
		<title>CRUD Suprimer un item</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    </head><body>
	     <div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="R-CRUD_index.php">
								<i class="fa fa-star fa-fw"></i>index
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <div class="cover">
            <div class="cover-image" style="background-image : url('bg.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>web service</h1>
                        <p class="text-danger">prestashop web service J4GUAR</p>
                        <br>
                        <br>					
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
				
				<?php
				//connection de l'api au webservice
				define('DEBUG', true);	// debeuguage 
				define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/');//lien de la boutique
				define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	//clé d'authentification récuperé par la liste
				require_once('./PSWebServiceLibrary.php'); //lien de la librairy du web service
				// On appel le webservice
				
				if (isset($_GET['DeleteID']))
				{
					echo '<h1>Customers Deletion</h1><br>';//titre
					echo '<a href="?">Retourner a la liste</a>';//lien de retour
					try
					{
						$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
						$webService->delete(array('resource' => 'customers', 'id' => intval($_GET['DeleteID'])));//met le code de supression dans une variable
						echo 'item bien suprimé !<meta http-equiv="refresh" content="5"/>';//raffraichie la page
					}
					catch (PrestaShopWebserviceException $e)
					{
						$trace = $e->getTrace();//affiche les erreurs
						if ($trace[0]['args'][0] == 404) echo 'Bad ID';
						else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
						else echo 'autre erreur<br />'.$e->getMessage();
					}
				}
				
				else
				{
					try
					{
						$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
						$opt = array('resource' => 'customers');//créer un tableau pour recuperer les customers
						$xml = $webService->get($opt);
						$resources = $xml->children()->children();//récupere le xml
					}
					
					catch (PrestaShopWebserviceException $e)
					{
						$trace = $e->getTrace();//affiche les erreurs
						if ($trace[0]['args'][0] == 404) echo 'Bad ID';
						else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
						else echo 'autre erreur';
					}
					
					echo '<h1>Liste des customers</h1>';//titre
					echo '<table border="5">';//tableau
					
					if (isset($resources))//si $resources existe alors 
					{
						echo '<tr>';//on commence le tableau
						if (!isset($DeletionID))//si $DeletionID n'existe pas alors 
						{
							echo '<th>Id</th><th>plus</th></tr>';
							foreach ($resources as $resource)//creation d'une boucle qui affiche le xml dans un tableau html
							{
								echo '<td>'.$resource->attributes().'</td><td>'.//affiche l'id des customers
								'<a href="?SuprimerID='.$resource->attributes().'">Suprimer</a>'.//suprimer les customers par leurs id
								'</td></tr>';
							}
						}
						echo '</table><br/>';//fin tableau
					}
				}
?>
</body></html>