<?php ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 ?>

<html><head>
		<title>CRUD import un item</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    </head><body>
	     <div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="R-CRUD_index.php">
								<i class="fa fa-star fa-fw"></i>index
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <div class="cover">
            <div class="cover-image" style="background-image : url('bg.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>web service</h1>
                        <p class="text-danger">prestashop web service J4GUAR</p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">



<form method="POST" action="R-CRUD_import.php" enctype="multipart/form-data">
     <!-- On limite le fichier à 100Ko -->
     <input type="hidden" name="MAX_FILE_SIZE" value="100000">
     Fichier : <input type="file" name="avatar"><br>
     <input type="submit" name="envoyer" value="Envoyer">
</form>

<?php

				$dossier = 'upload/';
				@$fichier = basename($_FILES['avatar']['name']);//cherche le fichier par le nom
				$taille_maxi = 100000;//definit la taille max
				@$taille = filesize($_FILES['avatar']['tmp_name']);//verifie la taille du fichier
				$extensions = array('.csv',".xml");//propose les extension xml et csv
				@$extension = strrchr($_FILES['avatar']['name'], '.');

				//Début des vérifications de sécurité...
				if(!in_array($extension, $extensions)) //Si l'extension n'est pas dans le tableau
				{
					 $erreur = 'Vous devez uploader un fichier de type csv<br>';
				}

				if($taille>$taille_maxi)
				{
					 $erreur = 'Le fichier est trop gros...';
				}

				if(!isset($erreur)) //S'il n'y a pas d'erreur, on upload
				{
					 //On formate le nom du fichier ici...
					 $fichier = strtr($fichier,
						  'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
						  'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
					 $fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);
					 if(move_uploaded_file($_FILES['avatar']['tmp_name'], $dossier . $fichier)) //Si la fonction renvoie TRUE, c'est que ça a fonctionné...
					 {
						  echo 'Upload effectué avec succès !<br>';
					 }
					 else //Sinon (la fonction renvoie FALSE).
					 {
						  echo 'Echec de l\'upload !';
					 }
				}

				else
				{
					 echo $erreur;
				}

				echo "contenu du fichier :<br>";
				// $read = readfile($_FILES['avatar']['name']);

				@$lines = file($_FILES['avatar']['name']);//met le fichier dans une variable
				echo "<br>";

				echo '<table border="5">';//creation de tableau html
				foreach ($lines as $line_num => $line)//créer une boucle
				{
					echo '<TR>';

					$split = explode(";", $line, -1);//split les valeurs dans le fichier
					for($i = 0; $i < count($split); $i++)//créer une boucle qui split
					{
						echo "<td>$split[$i]</Td>";//on affiche le resultat
					}
				}
				echo '</tr></table>';//fin du tableau html

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////                                                                                                                                          //////
	//////                                                          PARTIE PRESTASHOP                                                               //////                 
	//////                                                                                                                                          //////
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
				
				
	define('DEBUG', true);											// Debug mode
	define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop');		// Root path of your PrestaShop store http://www.myshop.com/
	define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	// Auth key (Get it in your Back Office)
	require_once('./PSWebServiceLibrary.php');

	//on recupere le fichier xml
	$monxml = simplexml_load_file("export_presta.xml");
	
	//début de la boucle pour envoyer les customers
	foreach ($monxml->children()->children() as $ressources)
	{

	
		try {
				$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG); //connection
				$xml = $webService->get(array('url' => PS_SHOP_PATH.'/api/customers?schema=synopsis'));//on recupere un shemas blanc xml

				// $ressources = $monxml->customers->children();//on recupere l'enfant 'customer' 

			

				

				
				//données obligatoires dans mon xml
				$lastname  					         	= $ressources->lastname;
				$firstname			  			      	= $ressources->firstname;
				$email     						        = $ressources->email;

				//autres données sans l'id dans mon xml
				$id_default_group  			     		= $ressources->id_default_group;
				$id_lang 						        = $ressources->id_lang;
				$newsletter_date_add   			  		= $ressources->newsletter_date_add;
				$ip_registration_newsletter  			= $ressources->ip_registration_newsletter;
				$last_passwd_gen 		    		 	= $ressources->last_passwd_gen;
				$secure_key     	     	   			= $ressources->secure_key;
				$deleted  			      	   			= $ressources->deleted;
				$passwd 					           	= $ressources->passwd;
				$id_gender     		     	 		   	= $ressources->id_gender;
				$birthday  				      	 		= $ressources->birthday;
				$newsletter 			     	 		= $ressources->newsletter;
				$optin     					  	    	= $ressources->optin;
				$website  					 	     	= $ressources->website;
				$company 						        = $ressources->company;
				$siret     						        = $ressources->siret;
				$ape  							        = $ressources->ape;
				$outstanding_allow_amount	  		 	= $ressources->outstanding_allow_amount;
				$show_public_prices    		  			= $ressources->show_public_prices;
				$id_risk  						        = $ressources->id_risk;
				$max_payment_days 		   			    = $ressources->max_payment_days;
				$active     			       		 	= $ressources->active;
				$note  						        	= $ressources->note;
				$is_guest 						        = $ressources->is_guest;
				$id_shop     				       	    = $ressources->id_shop;
				$id_shop_group     			   			= $ressources->id_shop_group;
				$date_add     					   	    = $ressources->date_add;
				$date_upd     					   	    = $ressources->date_upd;
						
					
				// var_dump($email);

				//on associe les données de mon xml au xml blank créer au debut
				$xml->customer->lastname 					            = $lastname;
				$xml->customer->firstname  					            = $firstname;
				$xml->customer->email 						            = $email;
				$xml->customer->id_default_group 			        	= $id_default_group;
				$xml->customer->id_lang  					            = $id_lang;
				$xml->customer->newsletter_date_add 		      		= $newsletter_date_add;
				$xml->customer->ip_registration_newsletter 	  			= $ip_registration_newsletter;
				$xml->customer->last_passwd_gen  			        	= $last_passwd_gen;
				$xml->customer->secure_key 					          	= $secure_key;
				$xml->customer->deleted 					            = $deleted;
				$xml->customer->passwd  					            = $passwd;
				$xml->customer->id_gender 					          	= $id_gender;
				$xml->customer->birthday 				            	= $birthday;
				$xml->customer->newsletter  				       		= $newsletter;
				$xml->customer->optin 						            = $optin;
				$xml->customer->website 					            = $website;
				$xml->customer->company  				            	= $company;
				$xml->customer->siret 					            	= $siret;
				$xml->customer->ape 					              	= $ape;
				$xml->customer->outstanding_allow_amount     			= $outstanding_allow_amount;
				$xml->customer->show_public_prices 	       				= $show_public_prices;
				$xml->customer->id_risk 			            		= $id_risk;
				$xml->customer->max_payment_days 	         			= $max_payment_days;
				$xml->customer->active 					             	= $active;
				$xml->customer->note 					               	= $note;
				$xml->customer->is_guest  			          			= $is_guest;
				$xml->customer->id_shop 				             	= $id_shop;
				$xml->customer->id_shop_group 			        		= $id_shop_group;
				$xml->customer->date_add  				        	   	= $date_add;
				$xml->customer->date_upd 				            	= $date_upd;
					
				
				$opt = array('resource' => 'customers');
				$opt['postXml'] = $xml->asXML();

				// var_dump($opt);
				
				//envoie le xml a prestashop
				$xml = $webService->add($opt);

					
			}	
			
			catch (PrestaShopWebserviceException $e)
			{
			  // Here we are dealing with errors
			  $trace = $e->getTrace();
			  if ($trace[0]['args'][0] == 404) echo 'Bad ID';
			  else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
			  else echo 'Other error<br />'.$e->getMessage();
			}
	
	}
	
	?>
	
</body></html>
