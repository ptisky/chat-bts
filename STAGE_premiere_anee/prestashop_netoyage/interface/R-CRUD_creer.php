<html><head>
		<title>CRUD creer un item</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    </head><body>
	     <div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="R-CRUD_index.php">
								<i class="fa fa-star fa-fw"></i>index
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <div class="cover">
            <div class="cover-image" style="background-image : url('bg.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>web service</h1>
                        <p class="text-danger">prestashop web service J4GUAR</p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
				
				<?php
				//connection de l'api au webservice
				define('DEBUG', true);	// debeuguage 
				define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/'); //lien de la boutique
				define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	//clé d'authentification
				require_once('./PSWebServiceLibrary.php'); //lien de la librairy du web service
				// On appel le webservice
				try
				{
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
					$opt = array('resource' => 'customers');//créer un tableau contenant les l'id des customers 
					if (isset($_GET['Create'])) 
						$xml = $webService->get(array('url' => PS_SHOP_PATH.'/api/customers?schema=blank')); //récupere le tableau xml de l'id selectionné 
					else
						$xml = $webService->get($opt); //on met les valeurs du tableau $opt dans $xml
					$resources = $xml->children()->children();
				}
				catch (PrestaShopWebserviceException $e)
				{
					$trace = $e->getTrace(); //affiche les erreurs
					if ($trace[0]['args'][0] == 404) echo 'Bad ID';
					else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
					else echo 'Other error<br />'.$e->getMessage();
				}
				
				if (count($_POST) > 0)//si on as plus de 0 resultat
				{
					foreach ($resources as $nodeKey => $node)//boucle qui met a jour les résultats
					{
						$resources->$nodeKey = $_POST[$nodeKey];
					}
					try
					{
						$opt = array('resource' => 'customers');//tableau des données de customers
						if ($_GET['Create'] == 'Creating')
						{
							$opt['postXml'] = $xml->asXML();//on transforme le tableau en xml
							$xml = $webService->add($opt);//on ajoute le xml a la boutique
							echo "Successfully added.";
						}
					}
					catch (PrestaShopWebserviceException $ex)
					{
						$trace = $ex->getTrace(); //on affiche les erreurs
						if ($trace[0]['args'][0] == 404) echo 'Bad ID';
						else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
						else echo 'Other error<br />'.$ex->getMessage();
					}
				}
				echo '<h1>Customer\'s ';//titre
				if (isset($_GET['Create'])) echo 'Creation';
				
				else echo 'List';		
				echo '</h1>';//fin du titre
				
				
				if (isset($_GET['Create']))//retour a la liste
					echo '<a href="?">retourner a la liste</a>';
					
				if (!isset($_GET['Create']))
					echo '<input type="button" onClick="document.location.href=\'?Create\'" value="Crée un produit ...">'; //bouton d'envoie du formulaire
				
				else
					echo '<form method="POST" action="?Create=Creating">';
				echo '<table border="5">';//créer un tableau
				
				if (isset($resources))
				{
					echo '<tr>';
					if (count($_GET) == 0)//si on as 0 résultat
					{
						echo '<th>Id</th></tr>';
						foreach ($resources as $resource) //créer un boucle qui affiche les id
						{
							echo '<tr><td>'.$resource->attributes().'</td></tr>';
						}
					}
					else
					{
						echo '</tr>';
						foreach ($resources as $key => $resource)//crée une boucle qui affiche le tableau hmtl de création
						{
							echo '<tr><th>'.$key.'</th><td>';
							if (isset($_GET['Create']))
								echo '<input type="text" name="'.$key.'" value=""/>';
							echo '</td></tr>';
						}
					}
				}
				echo '</table><br/>';//fin du tableau
				
				if (isset($_GET['Create']))
					echo '<input type="submit" value="Crée un produit ..."></form>';//bouton d'envoie du formulaire
?>
                   
        </div>
    

</body></html>