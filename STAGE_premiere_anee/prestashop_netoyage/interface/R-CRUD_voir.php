<html><head>
		<title>CRUD voir les items</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
        <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css" rel="stylesheet" type="text/css">
    </head><body>
	     <div class="navbar navbar-default navbar-static-top">
			<div class="container">
				<div class="navbar-header">
				</div>
				<div class="collapse navbar-collapse" id="navbar-ex-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="R-CRUD_index.php">
								<i class="fa fa-star fa-fw"></i>index
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <div class="cover">
            <div class="cover-image" style="background-image : url('bg.jpg')"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1>web service</h1>
                        <p class="text-danger">prestashop web service J4GUAR</p>
                        <br>
                        <br>						
                    </div>
                </div>
            </div>
        </div>
        <div class="section">
				<div class="row">
					<div class="col-md-12">
						<ul class="media-list">
							<li class="media">
							<a class="pull-left" href="http://127.0.0.1/modules/prestashop/R-CRUD_voirall.php">	
							<img class="media-object" src="1465305181_basics-08.png" height="64" width="64"></a>
							<div class="media-body">
								<h4 class="media-heading">Lister les items</h4> 
								<p>Permet de lister tout les item de la boutique</p>
							</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="media-list">
							<li class="media">
							<a class="pull-left" href="http://127.0.0.1/modules/prestashop/R-CRUD_modif.php">	
							<img class="media-object" src="1465314126_basics-30.png" height="64" width="64"></a>
							<div class="media-body">
								<h4 class="media-heading">modifier les items</h4> 
								<p>Permet de modifier tout les item de la boutique</p>
							</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<ul class="media-list">
							<li class="media">
							<a class="pull-left" href="http://127.0.0.1/modules/prestashop/R-CRUD_import.php">	
							<img class="media-object" src="1465316752_basics-17.png" height="64" width="64"></a>
							<div class="media-body">
								<h4 class="media-heading">importer les items</h4> 
								<p>Permet d'importer tout les item de la boutique en xml</p>
							</div>
							</li>
						</ul>
					</div>
				</div>		
				<div class="row">
					<div class="col-md-12">
						<ul class="media-list">
							<li class="media">
							<a class="pull-left" href="http://127.0.0.1/modules/prestashop/R-CRUD_export.php">	
							<img class="media-object" src="exp.png" height="64" width="64"></a>
							<div class="media-body">
								<h4 class="media-heading">exporter les items</h4> 
								<p>Permet d'exporter tout les item de la boutique en xml</p>
							</div>
							</li>
						</ul>
					</div>
				</div>						
<?php
				
				//connection de l'api au webservice
				define('DEBUG', true);	// debeuguage 
				define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/'); //lien de la boutique
				define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');	//clé d'authentification
				require_once('./PSWebServiceLibrary.php'); //lien de la librairy du web service
				// On appel le webservice
				try
				{
					$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
					$opt['resource'] = 'customers';//créer un tableau qui récupere les customers

					if (isset($_GET['id']))
						$opt['id'] = (int)$_GET['id'];
					// Call
					$xml = $webService->get($opt);

					$resources = $xml->children()->children();
				}
				
				catch (PrestaShopWebserviceException $e)
				{
					$trace = $e->getTrace();//on affiche les erreurs
					if ($trace[0]['args'][0] == 404) echo 'Bad ID';
					else if ($trace[0]['args'][0] == 401) echo 'Bad auth key';
					else echo 'Other error<br />'.$e->getMessage();
				}
				
				echo '<h1>Customers ';//titre
				
				if (isset($_GET['id']))
					echo 'Details';
				
				else
					echo 'List';
				
				echo '</h1>';//fin titre

				if (isset($_GET['id']))//boutton retour de liste
					echo '<a href="?">Retourner a la liste</a>';
					
				echo '<table border="5">';//créer un tableau

				if (isset($resources))
				{
					if (!isset($_GET['id']))
					{
						echo '<tr><th>Id</th><th>More</th></tr>';
						foreach ($resources as $resource)//boucle qui affiche les id
						{
							echo '<tr><td>'.$resource->attributes().'</td><td>'.//id
							'<a href="?id='.$resource->attributes().'">Afficher</a>'.//bouton qui renvoie a une page "?id=(id_du_produit)"
							'</td></tr>';
						}
					}
					else
					{
						foreach ($resources as $key => $resource)//créer une boucle qui affiche le resultat du customers selectionné
						{
							echo '<tr>';
							echo '<th>'.$key.'</th><td>'.$resource.'</td>';
							echo '</tr>';
						}
					}
				}
				echo '</table>';//fin du tableau 
?>
</body></html>