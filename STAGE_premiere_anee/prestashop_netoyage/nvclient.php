<?php
define('DEBUG', true);
define('PS_SHOP_PATH', 'http://127.0.0.1/modules/prestashop/');
define('PS_WS_AUTH_KEY', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y');
require_once('./PSWebServiceLibrary.php');
$psXML = <<<XML
<prestashop>
<cart>
  <id/>
  <id_address_delivery>3</id_address_delivery>
  <id_address_invoice>3</id_address_invoice>
  <id_currency>1</id_currency>
  <id_customer>2</id_customer>
  <id_guest>0</id_guest>
  <id_lang>2</id_lang>
  <id_carrier>8</id_carrier>
  <recyclable>1</recyclable>
  <gift>0</gift>
  <gift_message/>
  <associations>
   <cart_rows>
    <cart_row>
	 <id_product>10</id_product>
	 <id_product_attribute>39</id_product_attribute>
	 <quantity>1</quantity>
    </cart_row>
   </cart_rows>
  </associations>
</cart>
</prestashop>
XML;
$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
$xml = new SimpleXMLElement($psXML);
$opt = array( 'resource' => 'carts' );
$opt['postXml'] = $xml->asXML();
$xml = $webService->add( $opt );
?>