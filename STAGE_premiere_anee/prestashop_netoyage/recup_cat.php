<?php
class ps2
{
    public $client;

    public function __construct()
    {
        $this->getClient();
    }

    public function getClient()
    {
        try {
            $this->client = new PrestaShopWebservice('http://127.0.0.1/modules/prestashop/', '5RJ775NDQN2N9G4DUZ1USZP9LENL7B1Y', false);
        } catch (PrestaShopWebserviceException $ex) {
            echo 'error: <br />' . $ex->getMessage();
        }
    }
}

class ProductApi extends ps2
{
    public function findAll()
    {
        $categories = array();
        $opt['resource'] = 'categories';
        $opt['display'] = '[id,name]';
        $opt['limit'] = 1;
        $xml = $this->client->get($opt);
        $resources = $xml->categories->children();
        foreach ($resources as $resource)
            $categories[] = $resource->attributes();
        return $categories;
    }
}
?>