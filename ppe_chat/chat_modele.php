<?php
//connexion bdd
function bdd()
{
return $db = new PDO('mysql:host=localhost;dbname=chat', 'root', '');;
}
//ajouter un message
function ajout_message($bdd,$pseudo,$message)
{
    $req = $bdd->prepare("INSERT INTO message(Pseudo,Message,Date) VALUES(:Pseudo,:Message,NOW())");
    $req->execute(array("Pseudo"=>$pseudo,"Message"=>$message));
}
//afficher les messages
function message($bdd)
{
    $req = $bdd->query("SELECT * FROM message ORDER BY Date DESC");
     
    return $req;
}
//supprime un message au bout de 30 minutes
function expire_message($bdd)
{
     
    $req = $bdd->query("DELETE FROM message WHERE Date < DATE_SUB(NOW(), INTERVAL 30 MINUTE)");
     
}
 //calcule le pair/impaire -> utile pour le chat afficher 1 couleurs sur 2 
function pair($nombre)
{
    if ($nombre%2 == 0) return true;
    else return false;
}
 

?>