package partie_trois_et_quatre;

import java.util.Date;

public class Technicien extends Employe {
	
	final double FACTEUR_UNITE = 10;
	private int nbUnites;
	
	
	/**
	 * 
	 * @param t_Nom
	 * @param t_Prenom
	 * @param t_Age
	 * @param date
	 * @param tnbUnites
	 */
	public Technicien(String t_Nom, String t_Prenom, int t_Age, Date date, int tnbUnites)
	{
		super(t_Nom, t_Prenom, t_Age, date);
		this.nbUnites=tnbUnites;
	}
	
	
	@Override
	/**
	 * 
	 * @return salaire
	 */
	double calculerSalaire() {
		return FACTEUR_UNITE*nbUnites;
	}
	
	
	/**
	 * 
	 * @return titre
	 */
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}
	

}
