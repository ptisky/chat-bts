package partie_trois_et_quatre;

import java.util.ArrayList;

public class Personnel {
	private ArrayList<Employe> employer= new ArrayList<Employe>();
	final int MAXEMPLOYE = 10;
	
	
	/**
	 * 
	 * @param e
	 */
	public void ajouterEmploye(Employe e)
	{
		this.employer.add(e);
	}
	
	/**
	 * 
	 * @return salairemoyen
	 */
	public double salaireMoyen()
	{
		double temp=0;
		for(Employe e : employer)
		{
			temp = temp + e.calculerSalaire();
		}
		return temp/employer.size();
	}
	
	
	/**
	 * 
	 * @param e
	 */
	public void afficherSalaire(Employe e)
	{
		double salaire= e.calculerSalaire();
		System.out.println("je gagne "+salaire+" euros");
	}
	
	
	/**
	 * 
	 * @return salaireE
	 */
	public void afficheEmployerSalaire()
	{
		for(Employe e : employer)
		{
			double temp = e.calculerSalaire();
			System.out.println("je suis "+ e.getNom()+ " et je gagne "+ temp);
		}
	}
	
	
}
