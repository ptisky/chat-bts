package partie_trois_et_quatre;

import java.util.Date;

public class main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Personnel p = new Personnel();
		
		Date td = new Date(2000, 07, 12);
		TechARisque t = new TechARisque("manu", "elle", 24, td, 20);
		System.out.println("je suis "+t.getNom());
		System.out.println("je gagne "+t.calculerSalaire()+" en comptant ma prime :" +t.getPrimeRisque() + "\n");
		p.ajouterEmploye(t);
		
		Date md = new Date(2000, 07, 12);
		ManutARisque m = new ManutARisque("manu", "deux", 77, md, 88);
		System.out.println("je suis "+m.getNom());
		System.out.println("je gagne "+m.calculerSalaire()+" en comptant ma prime :" +m.getPrimeRisque() + "\n");
		p.ajouterEmploye(m);
		
		System.out.println("le salaire moyen est de "+p.salaireMoyen());

		

	}

}
