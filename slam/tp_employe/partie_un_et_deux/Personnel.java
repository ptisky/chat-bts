package partie_un_et_deux;

import java.util.ArrayList;

public class Personnel {
	
	private ArrayList<Employe> employer= new ArrayList<Employe>();
	final int MAXEMPLOYE = 50;
	
	
	/**
	 * 
	 * @param e
	 */
	public void ajouterEmploye(Employe e)
	{
		this.employer.add(e);
	}
	
	
	/**
	 * 
	 * @return Salairemoyen
	 */
	public double salaireMoyen()
	{
		double temp=0;
		for(Employe e : employer)
		{
			temp = temp + e.calculerSalaire();
		}
		return temp/employer.size();
	}
	
	
	/**
	 * 
	 * @param e
	 */
	public void afficherSalaire(Employe e)
	{
		double salaire= e.calculerSalaire();
		System.out.println("je gagne "+salaire+" euros");
	}
	
	
	
}
