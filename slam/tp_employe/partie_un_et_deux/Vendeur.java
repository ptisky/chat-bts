package partie_un_et_deux;

import java.util.Date;
/**
 * 
 * @author Morgan
 *
 */

public class Vendeur extends Commercial {
	
	final double POURCENT_VENDEUR = 1.2;
	final int BONUS_VENDEUR = 20;
	
	/**
	 * 
	 * @param v_nom
	 * @param v_prenom
	 * @param v_age
	 * @param v_date
	 * @param v_ChiffreAffaire
	 */
	public Vendeur(String v_nom, String v_prenom, int v_age, Date v_date, double v_ChiffreAffaire){
		super(v_nom, v_prenom, v_age, v_date, v_ChiffreAffaire);
	}

	
	/**
	 * @return salaire
	 */
	@Override
	double calculerSalaire() {
		return (this.chiffreAffaire*POURCENT_VENDEUR)+BONUS_VENDEUR;
	}
	
	
	/**
	 * @return Titre
	 */
	public String getTitre()
	{
		String temp = getClass().getName();
		temp = temp.substring(temp.indexOf(".")+1);
		return temp;
	}

}
