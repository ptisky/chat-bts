package partie_un_et_deux;

import java.util.Date;

public class main {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		
		Personnel p = new Personnel();
		
		Date t_date = new Date(2011, 07, 16);
		Date m_date = new Date(2017, 10, 10);
		Date v_date = new Date(2011, 11, 11);
		Date r_date = new Date(2012, 01, 01);

		
		Technicien t = new Technicien(" maurice", " numero 1", 19, t_date, 1200);
		Manutentionnaire m = new Manutentionnaire(" francois", " numero 1", 22, m_date, 2000);
		Vendeur v = new Vendeur(" kevin", " numero 1", 43, v_date, 150);
		representant r = new representant(" jesaispas", " numero 1", 55, r_date, 3500);
		

		double temp = v.getChiffreAffaire();
		temp = r.getChiffreAffaire();
		

		System.out.println("je suis le "+t.getNom()); //technicien
		p.ajouterEmploye(t);
		p.afficherSalaire(t);
		System.out.println("\n");
		
		System.out.println("je suis le "+m.getNom()); //Manutentionnaire
		p.ajouterEmploye(m);
		p.afficherSalaire(m);
		System.out.println("\n");
		
		System.out.println("je suis le "+v.getNom()); //Vendeur
		p.ajouterEmploye(v);
		p.afficherSalaire(v);
		System.out.println("\n");
		
		System.out.println("je suis le "+r.getNom() + " ,le chiffre d'affaire de ma boite est de "+temp +" euros"); //representant
		p.afficherSalaire(r);
		p.ajouterEmploye(r);
		System.out.println("\n");
		
		

		double moyenne = p.salaireMoyen();
		System.out.println("le salaire moyen est donc de "+moyenne+" euros");
	
	}
}